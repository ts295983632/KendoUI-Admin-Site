$(function () {
    // 获取数据源
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                $.fn.ajaxPost({
                    ajaxUrl: 'json/select_data.json',
                    succeed: function (res) {
                        options.success(res);
                    },
                    failed: function (res) {
                        options.error(res);
                    }
                });
            }
        },
        schema: {
            total: function (res) {
                return res.result.length;
            },
            data: 'data'
        },
        pageSize: 10
    });
    dataSource.read();
    dataSource.online(false);
    // 普通分页
    $('#generalPager').kendoPager({
        dataSource: dataSource
    });
    // 指定按钮数量分页
    $('#buttonCountPager').kendoPager({
        dataSource: dataSource,
        buttonCount: 5
    });
    // 输入框跳转分页
    $('#inputPager').kendoPager({
        dataSource: dataSource,
        input: true
    });
    // 选择页数量分页
    $('#pageSizesPager').kendoPager({
        dataSource: dataSource,
        pageSizes: true
    });
    // 刷新按钮分页
    $('#refreshPager').kendoPager({
        dataSource: dataSource,
        refresh: true
    });
    // 无信息分页
    $('#noInfoPager').kendoPager({
        dataSource: dataSource,
        info: false
    });
    // 无进退按钮分页
    $('#noPreviousNextPager').kendoPager({
        dataSource: dataSource,
        previousNext: false
    });
    // 无数字按钮分页
    $('#noNumericPager').kendoPager({
        dataSource: dataSource,
        numeric: false
    });
    // 自定义页数量分页
    $('#customPageSizesPager').kendoPager({
        dataSource: dataSource,
        pageSizes: [1, 2, 5, 10, 15, 20, 25, 30, 50, 60, 80, 100, 'all']
    });
    // 自定义数字按钮分页
    $('#customNumericPager').kendoPager({
        dataSource: dataSource,
        selectTemplate: '<li class="k-link"><span class="k-link k-state-selected rounded-circle">#=text#</span></li>',
        linkTemplate: '<li><a href="\\#" class="k-link rounded-circle" data-#= ns #page="#= idx #">#=text#</a></li>'
    });
    // 全功能分页
    $('#fullPager').kendoPager({
        dataSource: dataSource,
        buttonCount: 5,
        input: true,
        pageSizes: [5, 10, 15, 20, 25, 30, 50, 100, 'all'],
        refresh: true
    });
});