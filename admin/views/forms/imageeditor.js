$(function () {
    // 普通图像编辑框
    $('#generalImageEditor').kendoImageEditor({
        width: '100%',
        height: 800,
        imageUrl: path + 'img/lock_bg.jpg',
        saveAs: {
            fileName: 'image_edited.png'
        }
    });
});