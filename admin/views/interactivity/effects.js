$(function () {
    // 定义数据源
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                $.fn.ajaxPost({
                    ajaxUrl: 'json/list.json',
                    succeed: function (res) {
                        options.success(res);
                    },
                    failed: function (res) {
                        options.error(res);
                    }
                });
            }
        },
        schema: {
            data: 'data'
        }
    });
    // 定义模版
    var template =
        '<div class="listItem col-4 border-0 mb-3">' +
            '<img src="${ photo.url }">' +
            '<div class="intro">' +
                '<h6>${ realName }</h6>' +
                '<small class="font-weight-light text-white-50">${ nickName }</small>' +
            '</div>' +
        '</div>'
    // 水平展开特效
    $('#horizontalExpandEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).expand('horizontal').stop().play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).expand('horizontal').stop().reverse();
    });
    // 垂直展开特效
    $('#verticalExpandEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).expand('vertical').stop().play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).expand('vertical').stop().reverse();
    });
    // 淡入特效
    $('#fadeInEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).fade('in').startValue(0).duration(800).stop().play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).fade('in').startValue(0).duration(800).stop().reverse();
    });
    // 淡出特效
    $('#fadeOutEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).fade('out').endValue(0).duration(800).stop().play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).fade('out').endValue(0).duration(800).stop().reverse();
    });
    // 水平翻转特效
    $('#horizontalFlipEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget)).flip('horizontal', $(e.currentTarget).find('img'), $(e.currentTarget).find('.intro')).stop().play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget)).flip('horizontal', $(e.currentTarget).find('img'), $(e.currentTarget).find('.intro')).stop().reverse();
    });
    // 垂直翻转特效
    $('#verticalFlipEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget)).flip('vertical', $(e.currentTarget).find('img'), $(e.currentTarget).find('.intro')).stop().play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget)).flip('vertical', $(e.currentTarget).find('img'), $(e.currentTarget).find('.intro')).stop().reverse();
    });
    // 水平翻页特效
    $('#horizontalPageTurnEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template),
        dataBound: function (e) {
            fixSize(e.sender);
        }
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget)).pageturn('horizontal', $(e.currentTarget).find('img'), $(e.currentTarget).find('.intro')).duration(1000).stop().play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget)).pageturn('horizontal', $(e.currentTarget).find('img'), $(e.currentTarget).find('.intro')).duration(1000).stop().reverse();
    });
    // 垂直翻页特效
    $('#verticalPageTurnEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template),
        dataBound: function (e) {
            fixSize(e.sender);
        }
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget)).pageturn('vertical', $(e.currentTarget).find('img'), $(e.currentTarget).find('.intro')).duration(1000).stop().play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget)).pageturn('vertical', $(e.currentTarget).find('img'), $(e.currentTarget).find('.intro')).duration(1000).stop().reverse();
    });
    // 滑入上特效
    $('#upSlideInEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).slideIn('up').play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).slideIn('up').reverse();
    });
    // 滑入下特效
    $('#downSlideInEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).slideIn('down').play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).slideIn('down').reverse();
    });
    // 滑入左特效
    $('#leftSlideInEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).slideIn('left').play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).slideIn('left').reverse();
    });
    // 滑入右特效
    $('#rightSlideInEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).slideIn('right').play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).slideIn('right').reverse();
    });
    // 瓦片上特效
    $('#upTileEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).tile('up', $(e.currentTarget).find('img')).play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).tile('up', $(e.currentTarget).find('img')).reverse();
    });
    // 瓦片下特效
    $('#downTileEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).tile('down', $(e.currentTarget).find('img')).play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).tile('down', $(e.currentTarget).find('img')).reverse();
    });
    // 瓦片左特效
    $('#leftTileEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).tile('left', $(e.currentTarget).find('img')).play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).tile('left', $(e.currentTarget).find('img')).reverse();
    });
    // 瓦片右特效
    $('#rightTileEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template)
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).tile('right', $(e.currentTarget).find('img')).play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).tile('right', $(e.currentTarget).find('img')).reverse();
    });
    // 替换特效
    $('#replaceEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template),
        dataBound: function (e) {
            fixSize(e.sender);
        }
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('img')).replace($(e.currentTarget).find('.intro'), 'swap').run();
    });
    // 变形特效
    $('#transferEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template),
        dataBound: function (e) {
            fixSize(e.sender);
        }
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('img')).transfer($('#shoppingCart')).play().then(function () {
            $(this).fadeOut('slow', function () {
                $(this).remove();
            });
        });
        $(e.currentTarget).find('.intro').css('z-index', 1);
    });
    // 放大特效
    $('#zoomInEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template),
        dataBound: function (e) {
            fixSize(e.sender);
        }
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).zoom('in').startValue(0).duration(600).stop().play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).zoom('in').startValue(0).duration(600).stop().reverse();
    });
    // 缩小特效
    $('#zoomOutEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(template),
        dataBound: function (e) {
            fixSize(e.sender);
        }
    }).on('mouseenter', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).zoom('out').endValue(0).duration(600).stop().play();
    }).on('mouseleave', '.listItem', function (e) {
        kendo.fx($(e.currentTarget).find('.intro')).zoom('out').endValue(0).duration(600).stop().reverse();
    });
    // 自定义翻页特效
    $('#customPageTurnEffects').kendoListView({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/list.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        template: kendo.template(
            '<div class="pageItem">' +
                '<img class="img-thumbnail" src="${ photo.url }">' +
                '<picture style="background-image: url(\'${ photo.url }\');"></picture>' +
                '<div class="intro">' +
                    '<h5 class="mb-1">${ realName }<small class="font-weight-light text-white-50 ml-2">${ nickName }</small></h5>' +
                '</div>' +
            '</div>'
        ),
        dataBound: function (e) {
            $(e.sender.wrapper[0].children[0].children).last().addClass('current');
            $('#previous').hide();
        }
    }).data('kendoListView').dataSource.sort({
        field: 'id',
        dir: 'desc'
    });
    $('#previous').click(function (e) {
        e.preventDefault();
        var currentPage = $('#customPageTurnEffects .current');
        if (currentPage.next().next().length === 0) {
            $(this).hide();
        }
        kendo.fx($('#customPageTurnEffects')).pageturn('horizontal', currentPage.next(), currentPage).duration(800).stop().reverse().then(function () {
            currentPage.removeClass('current').next().addClass('current');
            $('#next').show();
        });
    });
    $('#next').click(function (e) {
        e.preventDefault();
        var currentPage = $('#customPageTurnEffects .current');
        if (currentPage.prev().prev().length === 0) {
            $(this).hide();
        }
        kendo.fx($('#customPageTurnEffects')).pageturn('horizontal', currentPage, currentPage.prev()).duration(800).stop().play().then(function () {
            currentPage.removeClass('current').prev().addClass('current');
            $('#previous').show();
        });
    });
    // 自定义替换特效
    $('#customReplaceEffects').kendoListView({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/list.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        template: kendo.template(
            '<div class="pageItem" data-id="${ id }">' +
                '<img class="img-thumbnail" src="${ photo.url }">' +
                '<picture style="background-image: url(\'${ photo.url }\');"></picture>' +
                '<div class="intro">' +
                    '<h5 class="mb-1">${ realName }<small class="font-weight-light text-white-50 ml-2">${ nickName }</small></h5>' +
                '</div>' +
            '</div>'
        ),
        dataBound: function (e) {
            $(e.sender.wrapper[0].children[0].children).hide().last().addClass('current');
        }
    }).data('kendoListView').dataSource.sort({
        field: 'id',
        dir: 'desc'
    });
    $('#customReplaceList').kendoListView({
        dataSource: dataSource,
        template: kendo.template(
            '<div class="media p-2" data-id="${ id }">' +
                '<img class="img-thumbnail w-30" src="${ photo.url }">' +
                '<div class="media-body align-self-center ml-3">' +
                    '<h5 class="font-weight-bold text-dark mb-2">${ realName }</h5>' +
                    '<small class="text-black-50">${ nickName }</small>' +
                '</div>' +
            '</div>'
        ),
        scrollable: true,
        selectable: true,
        change: function (e) {
            kendo.fx($('#customReplaceEffects .pageItem[data-id="' + this.select().attr('data-id') + '"]')).replace($('#customReplaceEffects .current'), 'swap').run();
            $('#customReplaceEffects .pageItem').removeClass('current');
            $('#customReplaceEffects .pageItem[data-id="' + this.select().attr('data-id') + '"]').addClass('current');
        },
        dataBound: function () {
            this.select(this.element.children().children().first());
        }
    });
    // 自定义综合特效
    $('#customCombinedEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(
            '<div class="listItem col-4 col-sm-2 border-0 mb-3">' +
                '<img src="${ photo.url }">' +
                '<div class="intro">' +
                    '<h6>${ realName }</h6>' +
                    '<small class="font-weight-light text-white-50">${ nickName }</small>' +
                '</div>' +
            '</div>'
        ),
        dataBound: function (e) {
            fixSize(e.sender);
        }
    }).on('mouseenter', '.listItem', function (e) {
        var fade = kendo.fx($(e.currentTarget).find('img')).fade('out').startValue(1).endValue(.3),
            zoom = kendo.fx($(e.currentTarget).find('img')).zoom('out').startValue(1).endValue(.5),
            flip = kendo.fx($(e.currentTarget)).flip('horizontal', $(e.currentTarget).find('img'), $(e.currentTarget).find('.intro'));
        fade.add(zoom).add(flip).duration(600).stop().play().then(function () {
            $(this).show();
            $(this).next().show();
        });
    }).on('mouseleave', '.listItem', function (e) {
        var fade = kendo.fx($(e.currentTarget).find('img')).fade('out').startValue(1).endValue(.3),
            zoom = kendo.fx($(e.currentTarget).find('img')).zoom('out').startValue(1).endValue(.5),
            flip = kendo.fx($(e.currentTarget)).flip('horizontal', $(e.currentTarget).find('img'), $(e.currentTarget).find('.intro'));
        fade.add(zoom).add(flip).duration(600).stop().reverse().then(function () {
            $(this).show();
            $(this).next().show();
        });
    });
    // 语义变焦特效
    var originWidth = 160,
        originHeight = 160,
        titleHeight = 40,
        fixWidth,
        fixHeight,
        wrapper = $('#semanticZoomEffects').parent(),
        wrapperWidth,
        wrapperHeight;
    $('#semanticZoomEffects').kendoListView({
        dataSource: dataSource,
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(
            '<div class="listItem col-4 col-sm-2 border-0 mb-3">' +
                '<img src="${ photo.url }">' +
                '<picture style="background-image: url(\'${ photo.url }\');"></picture>' +
                '<div class="intro">' +
                    '<h5 class="mb-1">${ realName }<small class="font-weight-light text-white-50 ml-2">${ nickName }</small></h5>' +
                '</div>' +
            '</div>'
        ),
        dataBound: function (e) {
            fixSize(e.sender);
            fixWidth = $(e.sender.wrapper[0].children[0].children[0]).width();
            fixHeight = $(e.sender.wrapper[0].children[0].children[0]).height();
            wrapperWidth = wrapper.outerWidth();
            wrapperHeight = wrapper.outerHeight();
            wrapper.css('overflow', 'hidden');
        }
    }).on('click', '.listItem', function (e) {
        var img = $(e.currentTarget).find('img'),
            imgWidth = img.outerWidth(),
            imgHeight = img.outerHeight(),
            imgPos = img.offset(),
            wrapperPos = wrapper.offset(),
            imgMoveLeft = (wrapperPos.left + (wrapperWidth - originWidth) / 2) - imgPos.left,
            imgMoveTop = (wrapperPos.top + (wrapperHeight - originHeight) / 2) - imgPos.top,
            titleMoveLeft = wrapperPos.left - imgPos.left,
            titleMoveTop = wrapperPos.top + wrapperHeight - titleHeight - imgPos.top;
        if (imgWidth === fixWidth && imgHeight === fixHeight) {
            img.addClass('img-thumbnail').css({
                'width': originWidth,
                'height': originHeight,
                'transform': 'translate(' + imgMoveLeft + 'px, ' + imgMoveTop + 'px)',
                'transition': 'all .3s ease-in-out',
                'z-index': 99
            }).next().css({
                'transition': 'all .3s ease-in-out',
                'opacity': 1,
                'z-index': 98
            }).next().css({
                'width': wrapperWidth,
                'height': titleHeight + 'px',
                'transform': 'translate(' + titleMoveLeft + 'px, ' + titleMoveTop + 'px)',
                'transition': 'all .3s ease-in-out',
                'opacity': 1,
                'z-index': 100
            });
        } else {
            img.removeClass('img-thumbnail').css({
                'width': fixWidth,
                'height': fixHeight,
                'transform': 'translate(0px, 0px)',
                'transition': 'all .3s ease-in-out',
                'z-index': 1
            }).next().css({
                'transition': 'all .3s ease-in-out',
                'opacity': 0,
                'z-index': -1
            }).next().css({
                'width': fixWidth,
                'height': fixHeight,
                'transform': 'translate(0px, 0px)',
                'transition': 'all .3s ease-in-out',
                'opacity': 0,
                'z-index': 0
            });
        }
    });
});

// 固定尺寸
function fixSize(sender) {
    var size = $(sender.wrapper[0].children[0].children[0]).width();
    $.each($(sender.wrapper[0].children[0].children), function () {
        $(this).width(size).height(size);
        $(this).find('img').width(size).height(size);
        $(this).find('.intro').width(size).height(size);
    });
}