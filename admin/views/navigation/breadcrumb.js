$(function () {
    // 普通面包屑
    $('#generalBreadcrumb').kendoBreadcrumb({
        items: [
            {
                type: 'rootitem',
                icon: 'home'
            },
            {
                type: 'item',
                text: '导航'
            },
            {
                type: 'item',
                text: '面包屑'
            }
        ],
        gap: 200
    });
    // 文字面包屑
    $('#textBreadcrumb').kendoBreadcrumb({
        items: [
            {
                type: 'rootitem',
                text: '首页',
                showText: true,
                showIcon: false
            },
            {
                type: 'item',
                text: '导航'
            },
            {
                type: 'item',
                text: '面包屑'
            }
        ]
    });
    // 图标面包屑
    $('#iconBreadcrumb').kendoBreadcrumb({
        items: [
            {
                type: 'rootitem',
                icon: 'home'
            },
            {
                type: 'item',
                icon: 'globe',
                showIcon: true
            },
            {
                type: 'item',
                icon: 'marker-pin',
                showIcon: true
            }
        ]
    });
    // 图标文字面包屑
    $('#iconTextBreadcrumb').kendoBreadcrumb({
        items: [
            {
                type: 'rootitem',
                icon: 'home',
                text: '首页',
                showText: true
            },
            {
                type: 'item',
                icon: 'globe',
                text: '导航',
                showIcon: true
            },
            {
                type: 'item',
                icon: 'marker-pin',
                text: '面包屑',
                showIcon: true
            }
        ]
    });
    // 根图标面包屑
    $('#rootBreadcrumb').kendoBreadcrumb({
        rootIcon: 'folder-open',
        items: [
            {
                type: 'rootitem'
            },
            {
                type: 'item',
                text: '导航'
            },
            {
                type: 'item',
                text: '面包屑'
            }
        ]
    });
    // 分隔符面包屑
    $('#delimiterBreadcrumb').kendoBreadcrumb({
        delimiterIcon: 'arrow-right',
        items: [
            {
                type: 'rootitem',
                icon: 'home'
            },
            {
                type: 'item',
                text: '导航'
            },
            {
                type: 'item',
                text: '面包屑'
            }
        ]
    });
    // 项目样式面包屑
    $('#itemClassBreadcrumb').kendoBreadcrumb({
        items: [
            {
                type: 'rootitem',
                icon: 'home',
                itemClass: 'theme-m'
            },
            {
                type: 'item',
                text: '导航',
                itemClass: 'font-weight-bold'
            },
            {
                type: 'item',
                text: '面包屑',
                itemClass: 'font-weight-bold'
            }
        ]
    });
    // 链接样式面包屑
    $('#linkClassBreadcrumb').kendoBreadcrumb({
        items: [
            {
                type: 'rootitem',
                icon: 'home',
                linkClass: 'theme-m-bg'
            },
            {
                type: 'item',
                text: '导航',
                linkClass: 'theme-m-bg'
            },
            {
                type: 'item',
                text: '面包屑',
                linkClass: 'theme-m-bg'
            }
        ]
    });
    // 图标样式面包屑
    $('#iconClassBreadcrumb').kendoBreadcrumb({
        rootIcon: '',
        items: [
            {
                type: 'rootitem',
                text: '首页',
                iconClass: 'mr-2 fas fa-home',
                showText: true
            },
            {
                type: 'item',
                text: '导航',
                iconClass: 'mr-2 fas fa-map-signs',
                showIcon: true
            },
            {
                type: 'item',
                text: '面包屑',
                iconClass: 'mr-2 breadcrumbIcon',
                showIcon: true
            }
        ]
    });
    // 链接面包屑
    $('#linkBreadcrumb').kendoBreadcrumb({
        navigational: true,
        items: [
            {
                type: 'rootitem',
                icon: 'home',
                href: 'admin/#/home'
            },
            {
                type: 'item',
                text: '导航',
                href: 'javascript:;'
            },
            {
                type: 'item',
                text: '面包屑',
                href: 'admin/#/navigation/breadcrumb'
            }
        ]
    });
    // 地址栏面包屑
    $('#locationBreadcrumb').kendoBreadcrumb({
        bindToLocation: true,
        navigational: true
    });
    // 默认值面包屑
    $('#defaultValueBreadcrumb').kendoBreadcrumb({
        value: '#/navigation/breadcrumb'
    });
    // 可编辑面包屑
    $('#editableBreadcrumb').kendoBreadcrumb({
        items: [
            {
                type: 'rootitem',
                text: 'home',
                showText: true
            },
            {
                type: 'item',
                text: 'navigation'
            },
            {
                type: 'item',
                icon: 'edit',
                text: 'breadcrumb',
                showIcon: true
            }
        ],
        editable: true
    });
});