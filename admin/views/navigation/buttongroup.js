$(function () {
    // DOM 按钮组
    $('#domButtonGroup').kendoButtonGroup();
    // 图标按钮组
    $('#iconButtonGroup').kendoButtonGroup({
        items: [
            {
                text: 'Angular',
                iconClass: 'fab fa-angular mr-1'
            },
            {
                text: 'React',
                iconClass: 'fab fa-react mr-1'
            },
            {
                text: 'Vue',
                iconClass: 'fab fa-vuejs mr-1'
            }
        ]
    });
    // 图片按钮组
    $('#imageButtonGroup').kendoButtonGroup({
        items: [
            {
                text: '艾欧里亚',
                imageUrl: 'img/temp/Leo.png'
            },
            {
                text: '沙加',
                imageUrl: 'img/temp/Virgo.png'
            },
            {
                text: '艾俄洛斯',
                imageUrl: 'img/temp/Sagittarius.png'
            }
        ]
    });
    // 单选按钮组
    $('#singleButtonGroup').kendoButtonGroup({
        items: [
            {
                text: 'HTML 5',
                iconClass: 'fab fa-html5 mr-1'
            },
            {
                text: 'CSS 3',
                iconClass: 'fab fa-css3-alt mr-1'
            },
            {
                text: 'JavaScript',
                iconClass: 'fab fa-js-square mr-1'
            }
        ],
        index: 0
    });
    // 多选按钮组
    $('#multipleButtonGroup').kendoButtonGroup({
        selection: 'multiple',
        items: [
            {
                text: 'HTML 5',
                iconClass: 'fab fa-html5 mr-1',
                selected: true
            },
            {
                text: 'CSS 3',
                iconClass: 'fab fa-css3-alt mr-1',
                selected: true
            },
            {
                text: 'JavaScript',
                iconClass: 'fab fa-js-square mr-1',
                selected: true
            }
        ]
    });
    // 禁用按钮组
    $('#disabledButtonGroup').kendoButtonGroup({
        items: [
            {
                text: 'HTML 5',
                iconClass: 'fab fa-html5 mr-1'
            },
            {
                text: 'CSS 3',
                iconClass: 'fab fa-css3-alt mr-1'
            },
            {
                text: 'JavaScript',
                iconClass: 'fab fa-js-square mr-1'
            }
        ],
        enable: false
    });
    // 按钮组格式
    $('#dataTypeButtonGroup').kendoButtonGroup({
        items: [
            {
                text: '普通按钮'
            },
            {
                text: '禁用按钮',
                enabled: false
            },
            {
                text: '添加样式',
                iconClass: 'fas fa-yin-yang mr-1'
            },
            {
                text: '添加属性',
                attributes: {
                    class: 'theme-s-bg'
                }
            },
            {
                text: '添加图标',
                icon: 'star'
            },
            {
                text: '添加图片',
                imageUrl: 'img/IKKI.png'
            },
            {
                text: '<i class="fas fa-smile mr-1"></i><strong>添加格式</strong>',
                encoded: false
            },
            {
                text: '添加徽标',
                badge: 88
            }
        ],
        select: function (e) {
            alertMsgNoBtn('你选择了<strong class="theme-m">' + this.current().text() + '</strong>~', 'info');
        }
    });
    // 徽标按钮组
    $('#shapeBadgeButtonGroup').kendoButtonGroup({
        items: [
            {
                text: '圆点',
                badge: {
                    shape: 'dot'
                }
            },
            {
                text: '圆形',
                badge: {
                    shape: 'circle',
                    text: 8
                }
            },
            {
                text: '胶囊',
                badge: {
                    shape: 'pill',
                    text: 8
                }
            },
            {
                text: '圆角',
                badge: {
                    shape: 'rounded',
                    text: 8
                }
            },
            {
                text: '直角',
                badge: {
                    shape: 'rectangle',
                    text: 8
                }
            },
            {
                text: '图标',
                badge: {
                    icon: 'heart'
                }
            }
        ]
    });
    $('#colorBadgeButtonGroup').kendoButtonGroup({
        items: [
            {
                text: '继承色',
                badge: {
                    themeColor: 'inherit',
                    text: 8
                }
            },
            {
                text: '默认色',
                badge: {
                    themeColor: 'default',
                    text: 8
                }
            },
            {
                text: '主色',
                badge: {
                    themeColor: 'primary',
                    text: 8
                }
            },
            {
                text: '次色',
                badge: {
                    themeColor: 'secondary',
                    text: 8
                }
            },
            {
                text: '第三色',
                badge: {
                    themeColor: 'tertiary',
                    text: 8
                }
            },
            {
                text: '信息',
                badge: {
                    themeColor: 'info',
                    text: 8
                }
            },
            {
                text: '成功',
                badge: {
                    themeColor: 'success',
                    text: 8
                }
            },
            {
                text: '警告',
                badge: {
                    themeColor: 'warning',
                    text: 8
                }
            },
            {
                text: '错误',
                badge: {
                    themeColor: 'error',
                    text: 8
                }
            },
            {
                text: '暗色',
                badge: {
                    themeColor: 'dark',
                    text: 8
                }
            },
            {
                text: '亮色',
                badge: {
                    themeColor: 'light',
                    text: 8
                }
            },
            {
                text: '反色',
                badge: {
                    themeColor: 'inverted',
                    text: 8
                }
            }
        ]
    });
    $('#styleBadgeButtonGroup').kendoButtonGroup({
        items: [
            {
                text: '实心',
                badge: {
                    fill: 'solid',
                    text: 8
                }
            },
            {
                text: '空心',
                badge: {
                    fill: 'outline',
                    text: 8
                }
            },
            {
                text: '描边',
                badge: {
                    cutoutBorder: true,
                    text: 8
                }
            }
        ]
    });
    $('#positionBadgeButtonGroup').kendoButtonGroup({
        items: [
            {
                text: '内嵌',
                badge: {
                    position: 'inline'
                }
            },
            {
                text: '左上角',
                badge: {
                    align: 'top start',
                    position: 'edge'
                }
            },
            {
                text: '右上角',
                badge: {
                    align: 'top end',
                    position: 'edge'
                }
            },
            {
                text: '左下角',
                badge: {
                    align: 'bottom start',
                    position: 'edge'
                }
            },
            {
                text: '右下角',
                badge: {
                    align: 'bottom end',
                    position: 'edge'
                }
            }
        ]
    });
    $('#placementBadgeButtonGroup').kendoButtonGroup({
        items: [
            {
                text: '边沿',
                badge: {
                    align: 'top end',
                    position: 'edge'
                }
            },
            {
                text: '内沿',
                badge: {
                    align: 'top end',
                    position: 'inside'
                }
            },
            {
                text: '外沿',
                badge: {
                    align: 'top end',
                    position: 'outside'
                }
            }
        ]
    });
    $('#sizeBadgeButtonGroup').kendoButtonGroup({
        items: [
            {
                text: '小',
                badge: {
                    size: 'small',
                    text: 8
                }
            },
            {
                text: '中',
                badge: {
                    size: 'medium',
                    text: 8
                }
            },
            {
                text: '大',
                badge: {
                    size: 'large',
                    text: 8
                }
            }
        ]
    });
    $('#otherBadgeButtonGroup').kendoButtonGroup({
        items: [
            {
                text: '最大值',
                badge: {
                    text: 100,
                    max: 99
                }
            },
            {
                text: '自定义',
                badge: {
                    data: {
                        current: 88,
                        total: 100
                    },
                    template: '#= current # of #= total #'
                }
            },
            {
                text: '隐藏',
                badge: {
                    text: 0,
                    visible: false
                }
            }
        ]
    });
});